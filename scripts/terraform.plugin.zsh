alias tf='terraform'

alias tfv='terraform validate'

alias tfin='terraform init'

alias tfp='terraform plan'

alias tfm='terraform fmt -recursive'

alias tfwst='terraform workspace select'

alias tfwsw='terraform workspace show'

alias tfwls='terraform workspace list'