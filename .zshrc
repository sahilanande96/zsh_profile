alias myip='curl http://ipecho.net/plain; echo'

alias distro='cat /etc/*-release'

alias reload='source ~/.zshrc'

alias sapu='sudo apt-get update'

alias ffs='sudo !!'

source ~/scripts/git.plugin.zsh
source ~/scripts/history.plugin.zsh
source ~/scripts/terraform.plugin.zsh
